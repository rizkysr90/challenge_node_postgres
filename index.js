// Connecting
const { Client } = require('pg');
const { Query } = require('pg/lib');

class QueryValidator {
  static validate(query,target) {
      const removeSpaces = query.replaceAll(' ','');
      let check = '';

      for (let i = 0; i < target.length; i++) {
        check += removeSpaces[i].toLowerCase();
      }
      return target === check;
  }
  static createTable(query) {
      return QueryValidator.validate(query,'createtable');
  }
  static createDatabase(query) {
      return QueryValidator.validate(query,'createdatabase');
  }
  static insertData(query) {
      return QueryValidator.validate(query,'insertinto');
  }
  static selectData(query) {
    return QueryValidator.validate(query,'select');
  }
  static updateData(query) {
    return QueryValidator.validate(query,'update');
  }
  static deleteData(query) {
    return QueryValidator.validate(query,'deletefrom');
  }
}
class SimpleApp {
    #email = 'rizkysr90@gmail.com';
    #password = '12345';
    count = 0;
    constructor(userObj) {
        this.client = new Client({
          user : 'postgres',
          host : 'localhost',
          password : 'adarizki123',
          port : 5432,
        });
        this.config = userObj.config;
        this.authenticated = this.#authenticate(userObj);
    }

    #authenticate(userInput){
      return this.#email === userInput.email && this.#password === userInput.password;
    }
    createDB(name,query) {
      if (QueryValidator.createDatabase(query)) {
        if (this.authenticated) {
          this.client.connect();
          return this.client
          .query(`DROP DATABASE IF EXISTS ${name};`)
          .then(() => this.client.query(query))
          .then(() => {
            this.count++;
            console.log(`${this.count}. SUCCESS CREATE DATABASE ${name}`);
            this.client.end();
            return Promise.resolve(true);
          })
          .catch(e => console.error(e.stack));
        }
      }
      throw new Error('INVALID QUERY');
    }
    useDB() {
      this.client = new Client(this.config);
      this.client.connect();
    }
    createTable(name,query) {
      if (QueryValidator.createTable(query)) {
          this.useDB();
          return this.client
          .query(query)
          .then(() => {
            this.count++;
            console.log(`${this.count}. SUCCESS CREATE TABLE ${name}`);
            this.client.end();
            return Promise.resolve(true);
          })
          .catch(e => console.error(e));
      } 
      
      throw new Error('INVALID QUERY');
      
    }
    insertData(name,query) {
      if (QueryValidator.insertData(query.text)) {
        this.useDB();
        return this.client
          .query(query)
          .then((res) => {
            this.count++;
            console.log(`${this.count}. SUCCESS INSERT ROW IN TABLE ${name}`);
            this.client.end();
            return Promise.resolve(true);
          })
          .catch(e => console.error(e));
      }
      throw new Error('INVALID QUERY');
    }
    selectRow(query) {
      if (QueryValidator.selectData(query)) {
        this.useDB();
        return this.client
          .query(query)
          .then((res) => {
            console.log(res.rows);
            this.client.end();
            return Promise.resolve(true);
          })
          .catch(e => console.error(e));
      }
      throw new Error('INVALID QUERY');  
    }
    updateData(name,query) {
      if (QueryValidator.updateData(query)) {
        this.useDB();
        return this.client
          .query(query)
          .then((res) => {
            this.count++;
            console.log(`${this.count}. SUCCESS UPDATE ROW IN TABLE ${name}`);
            this.client.end();
            return Promise.resolve(true);
          })
          .catch(e => console.error(e));
      }
      throw new Error('INVALID QUERY');
    }
    deleteData(name,query) {
      if (QueryValidator.deleteData(query)) {
        this.useDB();
        return this.client
          .query(query)
          .then((res) => {
            this.count++;
            console.log(`${this.count}. SUCCESS DELETE ROW IN TABLE ${name}`);
            this.client.end();
            return Promise.resolve(true);
          })
          .catch(e => console.error(e));
      }
      throw new Error('INVALID QUERY');
    }
   
    
}
// Change this to set up postgres in your local machine
const configDb = {
    user : 'postgres',
    host : 'localhost',
    password : 'adarizki123',
    database : 'db_ecommerce',
    port : 5432,
}
// Inisiating InstanceObject
const db = new SimpleApp({email : 'rizkysr90@gmail.com',password : '12345',config : configDb});
function start(){
  // Create Database
  const createDatabase = {
    name : configDb.database,
    query : `CREATE DATABASE ${configDb.database}`
  }
  db.createDB(createDatabase.name,createDatabase.query)
  .then(() => {
    // Create Table Customers
    const createTableCustAccounts = {
      name : 'customer_accounts',
      query :`CREATE TABLE customer_accounts (
        customer_account_id bigserial PRIMARY KEY,
        email text NOT NULL,
        password varchar(255) NOT NULL ,
        created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
        updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP);`
    }
    return db.createTable(createTableCustAccounts.name,createTableCustAccounts.query);
  })
  .then(() => {
    // Create Table Profile 
    const createTableProfiles = {
      name : 'profiles',
      query :`CREATE TABLE profiles (
        profile_id BIGSERIAL PRIMARY KEY,
        fullname VARCHAR(255) NOT NULL,
        phone  VARCHAR(50) NOT NULL,
        address TEXT NOT NULL,
        customer_account_id integer NOT NULL REFERENCES customer_accounts (customer_account_id),
        created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
        updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
        UNIQUE (customer_account_id));`
    }
    return db.createTable(createTableProfiles.name,createTableProfiles.query);
  })
  .then(() => {
    // Create Table Orders
    const createTableOrders = {
      name : 'orders',
      query :`CREATE TABLE orders (
        order_id bigserial PRIMARY KEY,
        customer_account_id integer NOT NULL REFERENCES customer_accounts (customer_account_id),
        created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
        updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP);`
    }
    return db.createTable(createTableOrders.name,createTableOrders.query);
  })
  .then(() => {
    // Create Table Products
    const createTableProducts = {
      name : 'products',
      query :`CREATE TABLE products (
        product_id bigserial PRIMARY KEY,
        name varchar(255) NOT NULL,
        price integer NOT NULL,
        stock integer NOT NULL,
        created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
        updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP);`
    }
    return db.createTable(createTableProducts.name,createTableProducts.query);
  })
  .then(() => {
    // Create Table Orders
    const createTableOrderDetails = {
      name : 'order_details',
      query :`CREATE TABLE order_details (
        order_id integer NOT NULL REFERENCES orders (order_id),
        product_id integer NOT NULL REFERENCES products (product_id),
        quantity integer NOT NULL,
        price integer NOT NULL,
        created_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
        updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (order_id,product_id));`
    }
    return db.createTable(createTableOrderDetails.name,createTableOrderDetails.query);
  })
  .then(() => {
    const insertCustomerAcc = {
      name : 'customer_accounts',
      query : {
        text : 'INSERT INTO customer_accounts (email,password) VALUES ($1, $2)',
        values : ['rizkyganteng@gmail.com','adaganteng123'],
      }
    }
    return db.insertData(insertCustomerAcc.name,insertCustomerAcc.query)
  })
  .then(() => {
    return db.selectRow('SELECT * FROM customer_accounts');
  })
  .then(() => {
    const insertProfiles = {
      name : 'profiles',
      query : {
        text : 'INSERT INTO profiles (fullname,phone,address,customer_account_id) VALUES ($1, $2, $3, $4)',
        values : ['Rizki Van Houtten','0898123456789','Jalan Cemara',1],
      }
    }
    return db.insertData(insertProfiles.name,insertProfiles.query)
  })
  .then(() => {
    return db.selectRow('SELECT * FROM profiles');
  })
  .then(() => {
    const insertOrders = {
      name : 'orders',
      query : {
        text : 'INSERT INTO orders (customer_account_id) VALUES ($1)',
        values : [1],
      }
    }
    return db.insertData(insertOrders.name,insertOrders.query)
  })
  .then(() => {
    return db.selectRow('SELECT * FROM orders');
  })
  .then(() => {
    const insertProducts = {
      name : 'products',
      query : {
        text : 'INSERT INTO products (name,price,stock) VALUES ($1,$2,$3)',
        values : ['Kaos Unik',50000,25],
      }
    }
    return db.insertData(insertProducts.name,insertProducts.query)
  })
  .then(() => {
    return db.selectRow('SELECT * FROM products');
  })
  .then(() => {
    const insertOrderDetails = {
      name : 'order_details',
      query : {
        text : 'INSERT INTO order_details (order_id,product_id,quantity,price) VALUES ($1,$2,$3,$4)',
        values : [1,1,2,100000],
      }
    }
    return db.insertData(insertOrderDetails.name,insertOrderDetails.query)
  })
  .then(() => {
    return db.selectRow('SELECT * FROM order_details');
  })
  .then(() => {
    const updateData = {
      name : 'order_details',
      query : 'UPDATE order_details SET quantity = 3, price = 150000 WHERE order_id = 1 AND product_ID = 1;'
    }
    return db.updateData(updateData.name,updateData.query)
  })
  .then(() => {
    return db.selectRow('SELECT * FROM order_details');
  })
  .then(() => {
    db.count++;
    console.log(`${db.count}. SUCCESS JOIN TABLE PROFILES WITH CUSTOMER ACCOUNTS`);
    return db.selectRow(`SELECT * FROM profiles p JOIN customer_accounts ca ON ca.customer_account_id = p.profile_id`);
  })
  .then(() => {
    const insertProducts = {
      name : 'products',
      query : {
        text : 'INSERT INTO products (name,price,stock) VALUES ($1,$2,$3)',
        values : ['Minyak Goreng Subsidi',28000,100],
      }
    }
    return db.insertData(insertProducts.name,insertProducts.query)
  })
  .then(() => {
    return db.selectRow('SELECT * FROM products');
  })
  .then(() => {
    const deleteData = {
      name : 'products',
      query : 'DELETE FROM products WHERE product_id = 2;'
    }
    return db.deleteData(deleteData.name,deleteData.query)
  })
  .then(() => {
    return db.selectRow('SELECT * FROM products');
  })
  .catch(e => console.error(e));
}
start();